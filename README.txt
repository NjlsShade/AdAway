# AdAway v0.0.1 -------------------------------------------------------#
#----------------------------------------------------------------------#
#---------------------------- Discription -----------------------------#
#                                                                      #
# AdAway is a hosts file based Ad-block program.                       #
#                                                                      #
# The hosts file is a list of mappings between hostnames and IPs.      #
# When a website calls on a blacklisted ad-site,                       #
# the request is directed to a null address of 0,                      #
# this in turn, shows you the resulting directed page,                 #
# which will be blank.                                                 #
#                                                                      #
#----------------------------------------------------------------------#
#----------------------------------------------------------------------#
#---------------------------- Requierments ----------------------------#
#                                                                      #
# wget (1.16->)                                                        #
# Internet connection                                                  #
# dialog (1.2->)                                                       #
# grep (2.16->)                                                        #
#                                                                      #
#----------------------------------------------------------------------#
#----------------------------------------------------------------------#
#--------------------------- To Install GUI ---------------------------#
#                                                                      #
# 1. Download AdAway.sh from the 'Downloads' section                   #
# 2. Right click on the download file and click properties             #
# 3. Click on the 'Permissions' tab and mark the file as executable    #
# 4. Double click AdAway.sh, and click, "Run in Terminal"              #
#                                                                      #
#----------------------------------------------------------------------#
#----------------------------------------------------------------------#
#--------------------------- To Install CLI ---------------------------#
#                                                                      #
# 1. wget https://bitbucket.org/NjlsShade/adaway/raw/master/AdAway.sh  #
# 2. sudo chmod 755 ./AdAway.sh                                        #
# 3. ./AdAway.sh                                                       #
#                                                                      #
#----------------------------------------------------------------------#
#----------------------------------------------------------------------#